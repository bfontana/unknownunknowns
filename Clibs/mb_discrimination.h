#ifndef mb_discrimination_h__
#define mb_discrimination_h__

typedef struct twoVectors twoVectors;

extern double _seed_rng();
extern double _calculate_L1_norm(twoVectors*);
extern double _uniform_random_sampling();
extern double _gaussian_random_sampling(double, double);
extern double mb_discrimination(int, int, double[][512], int, int);

#endif //mb_discrimination_h__
