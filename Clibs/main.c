#include <stdio.h>
#include <stdlib.h>
#include "mb_discrimination.h"

int main() {
  double data[10][512];
  for(int i=0; i<10; ++i) {
    for(int j=0; j<512; ++j) {
      data[i][j] = (i+1)*(j+1);
    }
  }

  /*for (int i=0; i<50000; ++i) {
    printf("%lf\n",_gaussian_random_sampling(0., 1.));
  }
  */
  mb_discrimination(10, 512, data, 30, 30);
  
  return 0;
}
