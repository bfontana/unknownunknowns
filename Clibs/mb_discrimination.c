#include <stdio.h> 
#include <stdlib.h> //srand, rand
#include <math.h> //log
#include <time.h> //time
#include <fcntl.h> //seed_rng()
#include <unistd.h> //seed_rng()

typedef struct twoVectors {
  int length;
  double *v1;
  double *v2;
} twoVectors;


// seed the random number generator rand()
// srand(time(NULL)) works only if random numbers are not requested within the same second
void _seed_rng(void)
{
  int fp = open("/dev/random", O_RDONLY);
  if (fp == -1) abort();
  unsigned seed;
  unsigned pos = 0;
  while (pos < sizeof(seed)) {
    int amt = read(fp, (char *) &seed + pos, sizeof(seed) - pos);
    if (amt <= 0) abort();
    pos += amt;
  }
  srand(seed);
  close(fp);
}

double _uniform_random_sampling() {
  srand(time(NULL));
  //_seed_rng();
  return (double)rand() / (double)RAND_MAX;
}

double _gaussian_random_sampling(double mean, double dev) {
  double x, w1, w2, r;
  do {
    w1 = 2.0 * _uniform_random_sampling() - 1.0; 
    w2 = 2.0 * _uniform_random_sampling() - 1.0;
    r = w1 * w1 + w2 * w2;
  } while( r >= 1.0 );
 
  r = sqrt( -2.0*log(r) / r );
  return( mean + w1 * r * dev ); 
}

//calculates the L1 norm between two vectors
//the received structure includes the length of the vectors, which is the same for both of them
double _calculate_L1_norm(twoVectors* v) {
  double res = 0.;
  printf("CHECK");
  for(int i=0; i<v->length; ++i) {
    res += abs(v->v1[i] - v->v2[i]);
  }
  printf("%lf", res);
  return res;
}

double mb_discrimination(int batch_size, int num_neurons, double data[batch_size][num_neurons], 
			 int Mrows, int Mcols) {
  double a=0.;

  double T[num_neurons * Mrows * Mcols];
  for(int iT=0; iT < num_neurons * Mrows * Mcols; ++iT) {
    T[iT] = _gaussian_random_sampling(0., 1.);
  }
  double M[batch_size * Mrows * Mcols];
  double O[batch_size * Mrows];

  twoVectors s;
  s.length = 3;
  double arr1[Mrows] = {1.,2.,3.};
  double arr1[Mrows] = {1.,2.,3.};
  s.v1 = arr;
  s.v2 = arr;
  _calculate_L1_norm(&s);
  
  /*
    T = np.random.normal(loc=0., scale=1., size=(num_neurons,Mrows,Mcols))
    M = np.zeros(shape=(batch_size,Mrows,Mcols), dtype=np.float32)
    O = np.zeros(shape=(batch_size,Mrows), dtype=np.float32)
    
    def _calculate_L1norm(v1, v2):
  return np.sum(np.absolute(v1 - v2))

    for batch_idx in range(batch_size):
  for row in range(Mrows):
  for col in range(Mcols):
  for neuron in range(num_neurons):
                    M[batch_idx][row][col] += x[batch_idx][neuron] * T[neuron][row][col]
                
		      for i in range(batch_size):
  for row in range(Mrows):
  for j in range(batch_size):
  O[i,row] += np.exp(-1 * _calculate_L1norm(M[i][row], M[j][row]))
                
    return np.concatenate((x, O), axis=1)
  */
  return a;
}
