import os
import time
import argparse
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from src.architecture import vgg16_1D
from src.data.spectra_generation import gen_spectrum_2lines as gen_data
from src.data.data import read_spectra_data as read_data
from src import argparser
from src.utilities import write_to_file

def predict(spectra_size):
    labels = [0, 1, 2, 3]

    checkpoint = '/fred/oz012/Bruno/checkpoints/'+str(FLAGS.checkpoint)+'/'
    with tf.Session() as sess:
        saver = tf.train.import_meta_graph(checkpoint+'-1000.data-00000-of-00001')
        saver.restore(sess, tf.train_latest_checkpoint('./'))
        
        graph = tf.get_default_graph()
        data_ph = graph.get_tensor_by_name('data_ph:2')
        logits = graph.get_tensor_by_name('logits:2')
        
        prediction_dataset = prediction_data(len(labels), spectra_size, labels)
        iterator = prediction_dataset.make_one_shot_iterator()
        next_element = iterator.get_next()

        init = tf.group(tf.local_variables_initializer(), tf.global_variables_initializer())
        sess.run(init)
        inputs, labels = sess.run(next_element)
        predictions = sess.run(logits, feed_dict={data_ph: inputs})
        
        pred_classes = tf.argmax(predictions, axis=1)
        pred_probas = tf.nn.softmax(predictions)
        
        print('Predicted classes: {}\nPredicted probabilities: {}'
              .format(pred_classes, pred_probas))

def train_evaluate(train_dataset_size, valid_dataset_size, batch_size, nepochs, nclasses, spectra_size):
    #the Dataset API takes care of the last batch (it is smaller than all the others)
    nbatches_t = int(np.ceil(train_dataset_size/batch_size))
    nbatches_v = int(np.ceil(valid_dataset_size/batch_size))

    training_dataset = train_data(batch_size, spectra_size)
    validation_dataset = evaluation_data(batch_size, spectra_size)
    
    handle = tf.placeholder(tf.string, shape=[])
    iterator = tf.data.Iterator.from_string_handle(
        handle, training_dataset.output_types, training_dataset.output_shapes)
    next_element = iterator.get_next()

    training_iterator = training_dataset.make_initializable_iterator()
    validation_iterator = validation_dataset.make_initializable_iterator()

    data_ph = tf.placeholder(tf.float32, shape=[None, spectra_size], name='data_ph')
    labels_ph = tf.placeholder(tf.int64, shape=[None], name='labels_ph')
    dropout_prob_ph = tf.placeholder_with_default(0.0, shape=())
    logits = vgg16_1D(data_ph, nclasses, dropout_prob_ph)
    loss = tf.losses.sparse_softmax_cross_entropy(labels=labels_ph, logits=logits)
    
    pred_classes = tf.argmax(logits, axis=1)
    
    with tf.name_scope('train'):
        acc_train, acc_train_op = tf.metrics.accuracy(labels=labels_ph, predictions=pred_classes)    
        optimizer = tf.train.AdamOptimizer(0.0001)
        train_op = optimizer.minimize(loss)
    vars_train_reset = [v for v in tf.local_variables() if 'train/' in v.name]

    with tf.name_scope('valid'):
        acc_valid, acc_valid_op = tf.metrics.accuracy(labels=labels_ph, predictions=pred_classes)    
    vars_valid_reset = [v for v in tf.local_variables() if 'valid/' in v.name]

    checkpoint = '/fred/oz012/Bruno/checkpoints/'+str(FLAGS.checkpoint)+'/'
    saver = tf.train.Saver(max_to_keep=5, keep_checkpoint_every_n_hours=1)

    with tf.Session() as sess:
        init = tf.group(tf.local_variables_initializer(), tf.global_variables_initializer())
        sess.run(init)

        training_handle = sess.run(training_iterator.string_handle())
        validation_handle = sess.run(validation_iterator.string_handle())

        #save the model      
        saver.save(sess, checkpoint, global_step=1000, write_meta_graph=False)

        for epoch in range(nepochs):
            #training
            try:
                sess.run(training_iterator.initializer)
                for batch_t in range(nbatches_t):
                    #reset training accuracy at the beginning of a new batch
                    sess.run(tf.variables_initializer(vars_train_reset))

                    inputs_t, labels_t = sess.run(next_element, feed_dict={handle: training_handle})
                    sess.run([train_op, acc_train_op], 
                             feed_dict={data_ph: inputs_t['data'], labels_ph:labels_t,
                                        dropout_prob_ph: 0.5})
                    loss_curr_train, acc_curr_train = sess.run([loss, acc_train], 
                             feed_dict={data_ph: inputs_t['data'], labels_ph:labels_t,
                                        dropout_prob_ph: 0.5})
                    if batch_t%50==0:
                        print('Train loss: {} Batch: {}'.format(loss_curr_train, batch_t))
                    if batch_t==nbatches_t-1:
                        print('Train accuracy: {}'.format(acc_curr_train))
            except tf.errors.OutOfRangeError:
                break

            #validation
            try:
                sess.run(validation_iterator.initializer)
                #reset validation accuracy only once per epoch (compare to train accuracy)
                sess.run(tf.variables_initializer(vars_valid_reset))

                for batch_v in range(nbatches_v):
                    inputs_v, labels_v = sess.run(next_element, feed_dict={handle: validation_handle})
                    sess.run(acc_valid_op, 
                            feed_dict={data_ph: inputs_v['data'], labels_ph:labels_v})

                acc_curr_valid, loss_curr_valid = sess.run([acc_valid,loss], 
                     feed_dict={data_ph: inputs_v['data'], labels_ph:labels_v})
                print('Accuracy: {}'.format(acc_curr_valid))
                print('Validation loss: {}'.format(loss_curr_valid))
            except tf.errors.OutOfRangeError:
                break
            
            #write metrics to file
            write_to_file('metrics.txt', [epoch+1],
                          [loss_curr_train], [acc_curr_train],
                          [loss_curr_valid], [acc_curr_valid])

def main(argv=None):
    start_time = time.time()
    nepochs = 10
    train_dataset_size = 200000
    valid_dataset_size = 40000
    batch_size = 2000
    spectra_size = 3500
    nclasses = 4

    if FLAGS.mode=='predict':
        predict(spectra_size)
    else:
        train_evaluate(train_dataset_size, valid_dataset_size, batch_size, nepochs, 
                       nclasses, spectra_size)

    print('Time taken: {:.2} seconds'.format(time.time() - start_time))


def train_data(batch_size, spectra_size):
    files = '/fred/oz012/Bruno/data/gen_fixed_ratio_6lines/gen.tfrecord'
    #files = '/fred/oz012/Bruno/data/gen_fixed_ratio/gen.tfrecord'
    dataset = read_data(files, spectra_size)
    return dataset.repeat(1).batch(batch_size)

def evaluation_data(batch_size, spectra_size):
    files = '/fred/oz012/Bruno/data/gen_fixed_ratio_6lines/gen_test.tfrecord'
    #files = '/fred/oz012/Bruno/data/gen_fixed_ratio/gen_test.tfrecord'
    dataset = read_data(files, spectra_size)
    return dataset.repeat(1).batch(batch_size)
            
def prediction_data(nsamples, spectra_size, labels):
    x, data, _ = gen_data(nsamples, spectra_size, labels)
    dataset = tf.data.Dataset.from_tensor_slices((dict(data=data)))
    return dataset.repeat(1).batch(1)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    FLAGS, _ = argparser.add_args(parser)
    tf.logging.set_verbosity(tf.logging.DEBUG)
    tf.app.run()
