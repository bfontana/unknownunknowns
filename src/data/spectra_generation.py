import numpy as np

def _gaussian_fn(x, mu, sigma):
    return np.exp(-(x - mu) * (x - mu) / (2 * sigma*sigma))

def _planck_fn(l, T):
    return 0.5*10**18 * l**-5. * ( 1/(np.exp(4000/l)-1 ))
    
def _noise_generator(size):
    """
    amplitudes = abs(np.random.normal(0.0, scale=1.2, size=size))
    noise = np.random.uniform(amplitudes, -amplitudes, size=size)
    """
    noise = np.random.normal(0.0, scale=1.2, size=size)
    return noise

def gen_spectrum_left_right(batch_size, length, modes=None):
    mode_dict = {0: 'left', 1: 'right'}
    modes_active = False
    if modes==None:
        x, y, modes = ([] for _ in range(3))
    else:
        modes_active = True
        if len(modes)!=batch_size:
            raise ValueError('Each value needs a mode (label)!')
        x, y = ([] for _ in range(2))
    for i in range(batch_size):
        if not modes_active:
            modes.append(np.random.randint(0,2))
        x.append(np.arange(length))
        noise = np.random.uniform(-1.,1., size=length)
        samples_per_tenth = int(length/10)
        variance = np.random.uniform(samples_per_tenth/30,3*samples_per_tenth/30,size=1)
        amplitude = 5.
        gaus = amplitude*_gaussian_fn(np.arange(samples_per_tenth), 
                              np.random.uniform(0, samples_per_tenth), 
                              variance)    
        if mode_dict[modes[i]]=='left':
            center = int(np.random.uniform(samples_per_tenth, 3*samples_per_tenth, size=1))
        elif mode_dict[modes[i]]=='right':
            center = int(np.random.uniform(7*samples_per_tenth, 9*samples_per_tenth, size=1))
        left_zeros = center-int(samples_per_tenth)
        right_zeros = length-(center+int(samples_per_tenth/2))
        gaus = np.insert(gaus, 0, np.zeros(center-int(samples_per_tenth/2)))
        gaus = np.append(gaus, np.zeros(length - (center+int(samples_per_tenth/2))))
        y.append(noise + gaus)
    return np.array(x), np.array(y), np.array(modes)

def gen_spectrum_4types(batch_size, length, modes=None):
    mode_dict = {0: 'left up', 1: 'right up', 2: 'left down', 3: 'right down'}
    modes_active = False
    if modes==None:
        x, y, modes = ([] for _ in range(3))
    else:
        modes_active = True
        if type(modes)==int:
            modes = [modes]
        if len(modes)!=batch_size:
            raise ValueError('Each value needs a mode (label)!')
        x, y = ([] for _ in range(2))
    for i in range(batch_size):
        if not modes_active:
            modes.append(np.random.randint(0,4))
        x.append(np.arange(1,length+1))
        noise = _noise_generator(size=length)
        samples_per_tenth = int(length/10)
        variance_limits = (samples_per_tenth/50,3*samples_per_tenth/50)
        amplitude_limits = (8,12)
        if mode_dict[modes[i]]=='left up' or mode_dict[modes[i]]=='right up':
            amplitude = amplitude
        elif mode_dict[modes[i]]=='left down' or mode_dict[modes[i]]=='right down':
            amplitude = (-amplitude_limits[1], -amplitude_limits[0])
        gaus = _gaussian_generator(samples_per_tenth, amplitude, variance_limits)

        if mode_dict[modes[i]]=='left up' or mode_dict[modes[i]]=='left down':
            center = int(np.random.uniform(samples_per_tenth, 3*samples_per_tenth, size=1))
        elif mode_dict[modes[i]]=='right up' or mode_dict[modes[i]]=='right down':
            center = int(np.random.uniform(7*samples_per_tenth, 9*samples_per_tenth, size=1))
        left_zeros = center-int(samples_per_tenth)
        right_zeros = length-(center+int(samples_per_tenth/2))
        gaus = np.insert(gaus, 0, np.zeros(center-int(samples_per_tenth/2)))
        gaus = np.append(gaus, np.zeros(length - (center+int(samples_per_tenth/2))))
        y.append(noise + gaus)
    return np.array(x), np.array(y), np.array(modes)

def gen_spectrum_2lines(batch_size, length, labels=None):
    labels_active = False
    unit_len = length/60
    labels_dict = {0: int(unit_len*2), 
                   1: int(unit_len*4),
                   2: int(unit_len*6),
                   3: int(unit_len*8)}
    if labels==None:
        labels = []
    else:
        labels_active = True
        if type(labels)==int:
            labels = [labels]
        if len(labels)!=batch_size:
            raise ValueError('Each value needs a mode (label)!')
    y = []
    x = np.arange(1,length+1)
    for i in range(batch_size):
        if not labels_active:
            labels.append(np.random.randint(0,4))
        noise = _noise_generator(size=length)

        variance_limits = (0.3*length/1000,5*length/1000)
        amplitude_limits = (3,6)

        gaus_number = 6
        variances = np.random.uniform(variance_limits[0],variance_limits[1],size=gaus_number)
        amplitudes = []
        for i_gaus in range(gaus_number):
            up_or_down = np.random.randint(0,2)
            if up_or_down:
                amplitude_limits = (-amplitude_limits[1], -amplitude_limits[0])
            if i_gaus==1: #fix the amplitude of second line relative to the first
                if amplitudes[0]>=0:
                    amplitudes.append(amplitudes[0]+3)
                else:
                    amplitudes.append(amplitudes[0]-3)
            else:
                amplitudes.append(np.random.uniform(amplitude_limits[0],amplitude_limits[1],size=1))

        gaussians = [amplitudes[j]*_gaussian_fn(np.arange(int(6*variances[j])), 
                                               np.random.uniform(0, int(6*variances[j])), 
                                               variances[j]) for j in range(gaus_number)]

        center1 = int(np.random.uniform(3*variances[0], 
                                        length-labels_dict[labels[i]]-3*variances[1], size=1))
        center2 = center1 + labels_dict[labels[i]]
        center3 = np.random.randint(np.ceil(3*variances[2]),np.floor(length-3*variances[2])+1)
        center4 = np.random.randint(np.ceil(3*variances[3]),np.floor(length-3*variances[3])+1)
        center5 = np.random.randint(np.ceil(3*variances[4]),np.floor(length-3*variances[4])+1)
        center6 = np.random.randint(np.ceil(3*variances[5]),np.floor(length-3*variances[5])+1)
        centers = np.array([center1, center2, center3, center4, center5, center6])

        """
        for k in range(gaus_number):
            left = centers[k] - int(3*variances[k])
            right = left + int(6*variances[k])
            noise[left:right] += gaussians[k]
        """
        #y.append(noise.astype(np.float32)+_planck_fn(x,3500))
        y.append(noise.astype(np.float32))
    return [x for i in range(batch_size)], np.array(y), np.array(labels)
