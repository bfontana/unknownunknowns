import numpy as np
from matplotlib import pyplot as plt

def write_to_file(fname, *args):
    with open(fname, 'a') as f:
        for x, *data in zip(*args):
            f.write('{},'.format(x))
            for i in range(len(args)-1):
                if i==len(args)-2:
                    f.write('{}\n'.format(data[i]))
                else:
                    f.write('{},'.format(data[i]))

def read_from_file(fname, splitter=','):
    with open(fname) as f:
        for values in f:
            values = values.split(splitter)
            ncols = len(values)
            break
        data = [[] for i in range(ncols)]
    with open(fname) as f:
        for values in f:
            values = values.replace('\n','').split(splitter)
            for i,val in enumerate(values):
                data[i].append(float(val))
    return [data[i] for i in range(ncols)]

def plot_gen_sample(sample, name=''):
    plt.plot(np.arange(3500), sample[0])
    #plt.show()
    plt.savefig('figs/{}.png'.format(name))
    plt.close()

def plot_gen_samples(sample, name=''):
    sample = sample[:24]
    nrows, ncols = 4, 6

    fig, axis = plt.subplots(nrows=nrows, ncols=ncols, figsize=(20,12))
    for irow in range(nrows):
        for icol in range(ncols):
            axis[irow,icol].plot(np.arange(3500), sample[irow*ncols+icol])
    fig.savefig('figs/{}.png'.format(name))
    plt.close()

def plot_mnist(samples, name):
    fig = plt.figure(figsize=(6,6))
    gs = gridspec.GridSpec(6,6)
    gs.update(wspace=0.05, hspace=0.05)
    
    for i, sample in enumerate(samples):
        ax = plt.subplot(gs[i])
        plt.axis('off')
        ax.set_xticklabels([])
        ax.set_yticklabels([])
        ax.set_aspect('equal')
        plt.imshow(sample.reshape(28,28), cmap='Greys_r')
    plt.savefig('figs/{}.png'.format(name))
    plt.close()


def log_tf_files(num_layers, loss, player='D'):
    gr = tf.get_default_graph()
    for i in range(num_layers):
        weight = gr.get_tensor_by_name(player+'/layer{}/kernel:0'.format(i + 1))
        grad = tf.gradients(loss, weight)[0]
        mean = tf.reduce_mean(tf.abs(grad))
        tf.summary.scalar(player+'_mean{}'.format(i + 1), mean)
        tf.summary.histogram(player+'_gradients{}'.format(i + 1), grad)
        tf.summary.histogram(player+'_weights{}'.format(i + 1), weight)
