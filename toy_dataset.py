import os
import time
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from src.architecture import small_1D_vgg16, medium_1D_vgg16
from src.data.spectra_generation import gen_spectrum_2lines as gen_data
from src.data.data import read_spectra_data as read_data
from src.hooks import AccLossHook

#os.environ['CUDA_VISIBLE_DEVICES']='-1'

TRAIN = tf.estimator.ModeKeys.TRAIN
EVAL = tf.estimator.ModeKeys.EVAL
PREDICT = tf.estimator.ModeKeys.PREDICT

def model_fn(features, labels, mode, params):
    logits = medium_1D_vgg16(features['data'], params['nclasses'])

    #predictions
    pred_classes = tf.argmax(logits, axis=1)
    pred_probas = tf.nn.softmax(logits)
    if mode == PREDICT:
        return tf.estimator.EstimatorSpec(mode, predictions=pred_classes)

    loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)
    optimizer = tf.train.AdamOptimizer()
    train_op = optimizer.minimize(loss, global_step=tf.train.get_global_step())

    acc = tf.metrics.accuracy(labels=labels, predictions=pred_classes)
    tf.summary.scalar('accuracy', acc[1])
    tf.summary.scalar('loss', loss)
    train_hook = tf.train.LoggingTensorHook(
        {'loss': loss, 'accuracy': acc[1]}, every_n_iter=50)

    return tf.estimator.EstimatorSpec(
        mode=mode,
        predictions=pred_classes,
        loss=loss,
        train_op=train_op,
        eval_metric_ops={'accuracy': acc},
        training_hooks = [train_hook])

def build_estimator(config, params):
    r = np.random.uniform(0,99999999)
    checkpoint = '/fred/oz012/Bruno/checkpoints/'+str(r)
    return tf.estimator.Estimator(model_fn=model_fn, 
                                  config=config,
                                  params=params,
                                  model_dir=checkpoint)

def main(argv=None):
    start_time = time.time()
    nepochs = 5
    batch_size = 100
    spectra_size = 3500

    config = tf.estimator.RunConfig()

    model = build_estimator(config=None,
                            params={'nclasses': 4})
    for i in range(nepochs):
        model.train(input_fn = lambda: input_fn_train(batch_size, nepochs, spectra_size))
        e = model.evaluate(input_fn = lambda: input_fn_evaluate(batch_size, spectra_size))
        print('Accuracy: {} Loss: {}'.format(e['accuracy'], e['loss']))

    """
    true_labels = [0, 1, 2, 3]
    p = model.predict(input_fn = lambda: input_fn_predict(len(true_labels), spectra_size, true_labels))
    for i,i_pred in enumerate(p):
        print("Prediction: {} True label: {}".format(i_pred, true_labels[i]))
    """
    print('Time taken: {:.2} seconds'.format(time.time() - start_time))

def input_fn_train(batch_size, nepochs, spectra_size):
    #files = '/fred/oz012/Bruno/data/gen_extra_lines/gen_spectra_plus_4_lines.tfrecord'
    #files = '/fred/oz012/Bruno/data/gen_fixed_ratio/gen_spectra_fixed_ratio.tfrecord'
    files = '/fred/oz012/Bruno/data/gen_blackbody/blackbody.tfrecord'
    dataset = read_data(files, spectra_size)
    return dataset.repeat(1).batch(batch_size)

def input_fn_evaluate(batch_size, spectra_size):
    #files = '/fred/oz012/Bruno/data/gen_extra_lines/gen_spectra_plus_4_lines_test.tfrecord'
    #files = '/fred/oz012/Bruno/data/gen_fixed_ratio/gen_spectra_fixed_ratio_test.tfrecord'
    files = '/fred/oz012/Bruno/data/gen_blackbody/blackbody_test.tfrecord'
    dataset = read_data(files, spectra_size)
    return dataset.repeat(1).batch(batch_size)

def input_fn_predict(nsamples, spectra_size, modes):
    x, data, _ = gen_data(nsamples, spectra_size, modes)
    """
    for it in range(4):
        plt.plot(x[it], data[it])
        plt.savefig(str(it)+'.png')
        plt.close()
    """
    dataset = tf.data.Dataset.from_tensor_slices((dict(data=data)))
    return dataset.batch(1)

if __name__ == '__main__':
    tf.logging.set_verbosity(tf.logging.DEBUG)
    tf.app.run()
