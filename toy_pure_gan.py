import os
import argparse
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from src.architecture import dcgan_discriminator, dcgan_generator
from src.data.spectra_generation import gen_spectrum_2lines as gen_data
from src.data.data import read_spectra_data as read_data
from src import argparser
from src.utilities import write_to_file, plot_gen_samples

def noise(m,n):
    return np.random.normal(loc=0.0, scale=1., size=[m,n])

def predict():
    labels = [0, 1, 2, 3]
    spectra_size = 3500

    checkpoint = '/fred/oz012/Bruno/checkpoints/'+str(FLAGS.checkpoint)+'/'
    with tf.Session() as sess:
        saver = tf.train.import_met_graph(checkpoint+'.meta')
        saver.restore(sess, tf.train_latest_checkpoint('./'))
        
        graph = tf.get_default_graph()
        data_ph = graph.get_tensor_by_name('data_ph:2')
        logits = graph.get_tensor_by_name('logits:2')
        
        prediction_dataset = prediction_data(len(labels), spectra_size, labels)
        iterator = prediction_dataset.make_one_shot_iterator()
        next_element = iterator.get_next()

        init = tf.group(tf.local_variables_initializer(), tf.global_variables_initializer())
        sess.run(init)
        inputs, labels = sess.run(next_element)
        predictions = sess.run(logits, feed_dict={data_ph: inputs})
        
        pred_classes = tf.argmax(predictions, axis=1)
        pred_probas = tf.nn.softmax(predictions)
        
        print('Predicted classes: {}\nPredicted probabilities: {}'
              .format(pred_classes, pred_probas))

def train(dataset_size, batch_size, nepochs, spectra_size, noise_size):
    #the Dataset API takes care of the last batch (it is smaller than all the others)
    nbatches = int(np.ceil(dataset_size/batch_size))

    training_dataset = train_data(batch_size, spectra_size)
    validation_dataset = evaluation_data(batch_size, spectra_size)
    
    handle = tf.placeholder(tf.string, shape=[])
    iterator = tf.data.Iterator.from_string_handle(
        handle, training_dataset.output_types, training_dataset.output_shapes)
    next_element = iterator.get_next()

    training_iterator = training_dataset.make_initializable_iterator()
    validation_iterator = validation_dataset.make_initializable_iterator()

    dropout_prob_ph = tf.placeholder_with_default(0.0, shape=())
    dropout_prob_D = 0.7
    dropout_prob_G = 0.3

    with tf.variable_scope('G'):
        gen_data_ph = tf.placeholder(tf.float32, shape=[None, noise_size], name='gen_data_ph')
        G_sample = dcgan_generator(gen_data_ph, spectra_size, dropout_prob_ph)

    with tf.variable_scope('D') as scope:
        data_ph = tf.placeholder(tf.float32, shape=[None, spectra_size], name='data_ph')   
        D_real_logits, D_real = dcgan_discriminator(data_ph, dropout_prob_ph)
    with tf.variable_scope(scope, reuse=True):
        D_fake_logits, D_fake = dcgan_discriminator(G_sample, dropout_prob_ph)

    smooth = .95
    cross_entropy = tf.nn.sigmoid_cross_entropy_with_logits
    D_loss = tf.reduce_mean(cross_entropy(logits=D_real_logits, labels=tf.constant(smooth, shape=[batch_size, 1])) + cross_entropy(logits=D_fake_logits, labels=tf.zeros_like(D_fake_logits)))
    G_loss = tf.reduce_mean(cross_entropy(logits=D_fake_logits, labels=tf.constant(smooth, shape=[batch_size, 1])))
    D_optimizer = tf.train.AdamOptimizer(0.0002, beta1=0.9)
    G_optimizer = tf.train.AdamOptimizer(0.0002, beta1=0.9)
    D_train_op = D_optimizer.minimize(D_loss)
    G_train_op = G_optimizer.minimize(G_loss)

    #metrics
    pred_classes = tf.concat([tf.argmax(D_real, axis=1),tf.argmax(D_fake, axis=1)], axis=0)
    labels = tf.constant([1., 0.]*batch_size, shape=[2*batch_size])
    with tf.name_scope('metrics'):
        D_acc_train, D_acc_train_op = tf.metrics.accuracy(labels=labels, predictions=pred_classes)    
    vars_train_reset = [v for v in tf.local_variables() if 'metrics/' in v.name]

    checkpoint = '/fred/oz012/Bruno/checkpoints/'+str(FLAGS.checkpoint)+'/'
    saver = tf.train.Saver(max_to_keep=5, keep_checkpoint_every_n_hours=1)

    with tf.Session() as sess:
        init = tf.group(tf.local_variables_initializer(), tf.global_variables_initializer())
        sess.run(init)

        training_handle = sess.run(training_iterator.string_handle())
        validation_handle = sess.run(validation_iterator.string_handle())

        #save the model      
        saver.save(sess, checkpoint, global_step=1000, write_meta_graph=False)

        for epoch in range(nepochs):
            try:
                sess.run(training_iterator.initializer)
                for batch in range(nbatches):
                    sess.run(tf.variables_initializer(vars_train_reset))

                    inputs, _ = sess.run(next_element, feed_dict={handle: training_handle})

                    #train discriminator
                    noise_D = noise(batch_size,noise_size)
                    sess.run([D_train_op, D_acc_train_op], 
                             feed_dict={data_ph:inputs['data'], gen_data_ph: noise_D,
                                        dropout_prob_ph: dropout_prob_D})
                    D_real_c, D_fake_c = sess.run([D_real, D_fake],
                             feed_dict={data_ph:inputs['data'], gen_data_ph: noise_D,
                                        dropout_prob_ph: dropout_prob_D})
                    D_loss_curr_train, D_acc_curr_train = sess.run([D_loss, D_acc_train], 
                             feed_dict={data_ph:inputs['data'], gen_data_ph: noise_D,
                                        dropout_prob_ph: dropout_prob_D})
                    #train generator
                    noise_G = noise(batch_size,noise_size)
                    _, G_loss_curr_train = sess.run([G_train_op, G_loss], 
                             feed_dict={gen_data_ph: noise_G,
                                        dropout_prob_ph: dropout_prob_G})
                    
                    if True:
                        print('D loss: {} G loss: {} Batch: {}'
                              .format(D_loss_curr_train, G_loss_curr_train, batch))
                        print('D real mean: {} D fake mean: {} \n D real: {} D fake {} Batch: {}'
                              .format(np.mean(D_real_c), np.mean(D_fake_c), 
                                      D_real_c[0:6], D_fake_c[0:6], batch))
                        print('D accuracy: {}'.format(D_acc_curr_train))
                        print()
                        write_to_file('metrics_gan.txt', [(epoch+1)*nbatches+(batch+1)],
                                      [G_loss_curr_train], [D_loss_curr_train],
                                      [np.mean(D_real_c)], [np.mean(D_fake_c)])

                    if batch==50 or batch==99:
                        sample = sess.run(G_sample, feed_dict={gen_data_ph: noise_G,
                                                               dropout_prob_ph: dropout_prob_G})
                        plot_gen_samples(sample, 'gen'+str(epoch)+'_'+str(batch))
                        plot_gen_samples(inputs['data'], 'data'+str(epoch)+'_'+str(batch))

                        
            except tf.errors.OutOfRangeError:
                break


def main(argv=None):
    nepochs = 300
    dataset_size = 200000
    batch_size = 2000
    spectra_size = 3500
    noise_size = 100

    train(dataset_size, batch_size, nepochs, spectra_size, noise_size)


def train_data(batch_size, spectra_size):
    files = '/fred/oz012/Bruno/data/gen_fixed_ratio_tanh/gen.tfrecord'
    dataset = read_data(files, spectra_size)
    return dataset.repeat(1).batch(batch_size)

def evaluation_data(batch_size, spectra_size):
    files = '/fred/oz012/Bruno/data/gen_fixed_ratio_tanh/gen_test.tfrecord'
    dataset = read_data(files, spectra_size)
    return dataset.repeat(1).batch(batch_size)
            
def prediction_data(nsamples, spectra_size, labels):
    x, data, _ = gen_data(nsamples, spectra_size, labels)
    dataset = tf.data.Dataset.from_tensor_slices((dict(data=data)))
    return dataset.repeat(1).batch(1)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    FLAGS, _ = argparser.add_args(parser)
    tf.logging.set_verbosity(tf.logging.DEBUG)
    tf.app.run()
